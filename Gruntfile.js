module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-concat-css');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-connect');

  grunt.initConfig({
    clean: {
      css: ['assets/styles/styles.css', 'assets/styles/styles.min.css'],
    },

    concat_css: {
      options: {
        // Task-specific options go here.
      },
      all: {
        src: ["assets/styles/*.css"],
        dest: "assets/styles/styles.css"
      },
    },

    cssmin: {
      target: {
        files: [{
          expand: true,
          src: ['assets/styles/styles.css'],
          dest: '',
          ext: '.min.css'
        }]
      }
    },

    htmlmin: {
      dist: {
        options: {
          removeComments: true,
          collapseWhitespace: true
        },
        files: {
          'dist/index.html': 'index.html',
          'dist/mobile-view.html': 'mobile-view.html',
          'dist/desktop-view.html': 'desktop-view.html'
        }
      }
    },

    connect: {
      server: {
        options: {
          port: 9000,
          keepalive: true,
          // base: 'dist'
        }
      }
    }
  });

  grunt.registerTask('default', ['concat_css', 'cssmin', 'connect']);
  grunt.registerTask('build', ['clean', 'concat_css', 'cssmin']);
};